export const theme = {
	colors: {
		primary: '#FFE44D',
		green: '#01A54E',
		white: '#FFF',
		black: '#1A1A1A',
		outlineText: '#CFD8DC',
		red: '#F44336',
		secondary: '#7B8E98',
	},
	sizes: {
		sm: '8px',
		xsm: '12px',
		md: '16px',
		lg: '24px',
		xmd: '20px',
	},
	fontWeight: {
		sm: 400,
		md: 600,
		lg: 700,
		xlg: 800,
	},
};

export const footerLinks = [
	{
		title: 'Каталог',
		url: '/catalog',
	},
	{
		title: 'Акции',
		url: '/',
	},
	{
		title: 'Новости',
		url: '/',
	},
	{
		title: 'Отзывы',
		url: '/',
	},
	{
		title: 'Оплата и доставка',
		url: '/',
	},
	{
		title: 'Часто спрашивают',
		url: '/',
	},
	{
		title: 'Обратная связь',
		url: '/',
	},
	{
		title: 'Контакты',
		url: '/',
	},
];

export const footerSocialNetworks = [
	{
		iconName: 'telegram',
		url: '',
	},
	{
		iconName: 'whatsApp',
		url: '',
	},
	{
		iconName: 'viber',
		url: '',
	},
	{
		iconName: 'instagram',
		url: '',
	},
	{
		iconName: 'vk',
		url: '',
	},
];

export const sortData = [
	{ title: 'Популярные', code: 'popular' },
	{ title: 'Новинки', code: 'news' },
	{ title: 'Сначала дешёвые', code: 'minPrice' },
	{ title: 'Сначала дорогие', code: 'maxPrice' },
	{ title: 'По рейтингу', code: 'reit' },
	{ title: 'По скидке', code: 'sale' },
];

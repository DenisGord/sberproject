import { createRoot } from 'react-dom/client';
import { App } from './app';
import { StrictMode } from 'react';
import { ThemeProvider } from 'styled-components';
import { theme } from './constants';

const domNode = document.getElementById('root') as HTMLDivElement;
const root = createRoot(domNode);
root.render(
	<StrictMode>
		<ThemeProvider theme={theme}>
			<App />
		</ThemeProvider>
	</StrictMode>
);

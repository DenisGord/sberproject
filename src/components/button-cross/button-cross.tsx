import React, { FC } from 'react';
import { ButtonCrossStyle } from './button-cross.style';
import { Icon } from '../icon';

interface ButtonCrossProps {
	onClick: () => void;
}
export const ButtonCross: FC<ButtonCrossProps> = ({ onClick }) => {
	return (
		<ButtonCrossStyle onClick={onClick}>
			<Icon name={'icCloseInput'} />
		</ButtonCrossStyle>
	);
};

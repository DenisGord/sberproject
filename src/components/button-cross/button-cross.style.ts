import styled from 'styled-components';

export const ButtonCrossStyle = styled.button`
	width: 16px;
	height: 16px;
	background-color: #cfd8dc;
	color: white;
	border: none;
	border-radius: 50%;
	display: flex;
	align-items: center;
	justify-content: center;
`;

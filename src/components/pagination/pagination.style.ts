import styled from 'styled-components';

interface Props {
	active?: boolean;
}
export const PaginationButtonStyle = styled.button<Props>(
	({ theme, active }) => `
	border-radius: 50%;
	background:${active ? theme.colors.primary : theme.colors.white};
	border:${active ? 'none' : `1px solid ${theme.colors.secondary}`};
	width: 48px;
	height:48px;
	font-size: ${theme.sizes.md};
	font-weight:${theme.fontWeight.md};
	cursor:pointer;
`
);

export const PaginationStyle = styled.div`
	display: flex;
	align-items: center;
	justify-content: space-between;
	width: 100%;
`;

export const ButtonListStyle = styled.div`
	display: flex;
	gap: 12px;
`;

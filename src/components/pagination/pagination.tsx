import React, { FC, useCallback, useMemo } from 'react';
import {
	ButtonListStyle,
	PaginationButtonStyle,
	PaginationStyle,
} from './pagination.style';
import { ButtonWhite } from '../button-white';

interface PaginationProps {
	quantity: number;
	setNum: React.Dispatch<React.SetStateAction<number>>;
	current: number;
}
export const Pagination: FC<PaginationProps> = ({
	quantity,
	setNum,
	current,
}) => {
	const prevClick = useCallback(() => {
		setNum((prev: number) => {
			if (prev !== 0) {
				return prev - 1;
			}
			return prev;
		});
	}, [setNum]);

	const nextClick = useCallback(() => {
		setNum((prev: number) => {
			if (prev !== Math.floor(quantity / 12)) {
				return prev + 1;
			}
			return prev;
		});
	}, [quantity, setNum]);

	const buttonList = useMemo(() => {
		const arr = [];

		for (let i = 0; i < Math.ceil(quantity / 12); i++) {
			arr.push(
				<PaginationButtonStyle
					active={current === i}
					key={i}
					onClick={() => setNum(i)}>
					{i + 1}
				</PaginationButtonStyle>
			);
		}
		return arr;
	}, [current, quantity, setNum]);

	return (
		<PaginationStyle>
			<ButtonWhite onClick={prevClick} content={'Назад'} />
			<ButtonListStyle>{buttonList}</ButtonListStyle>
			<ButtonWhite onClick={nextClick} content={'Вперед'} />
		</PaginationStyle>
	);
};

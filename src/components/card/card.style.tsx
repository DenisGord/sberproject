import styled from 'styled-components';

export const CardStyle = styled.div(
	({ theme }) => `
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: flex-start;
	gap: ${theme.sizes.md};
	width:236px;
	position:relative;
	margin-top:auto;
`
);

export const PercentStyle = styled.div(
	({ theme }) => `
	position:absolute;
	background:${theme.colors.red};
	color:${theme.colors.white};
	border-radius:${theme.sizes.xmd};
	font-size:${theme.sizes.md};
	font-weight:${theme.fontWeight.xlg};
	display:flex;
	align-items:center;
	justify-content:center;
	padding:2px 8px;
	top:0;
	left:0;
	z-index:1;
`
);

export const FavoritesStyle = styled.div`
	position: absolute;
	top: 0;
	right: 0;
	z-index: 1;
	cursor: pointer;
`;

export const OldPriceStyle = styled.p(
	({ theme }) => `
	font-size: ${theme.sizes.xsm};
	font-weight:${theme.fontWeight.md};
	text-decoration:line-through;
	font-style: normal;
	margin:0;
`
);

export const NameStyle = styled.div`
	padding-right: 20px;
`;

export const ButtonBagStyle = styled.button(
	({ theme }) => `
	background:${theme.colors.primary};
	display:flex;
	align-items:center;
	justify-content:center;
	font-size: ${theme.sizes.md};
	font-weight:${theme.fontWeight.md};
	border-radius:55px;
	padding: 10px 18px;
	border:none;
	cursor: pointer;
`
);

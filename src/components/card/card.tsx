import React, { FC } from 'react';
import { Typography } from '../typography';
import { Icon } from '../icon';
import {
	CardStyle,
	FavoritesStyle,
	PercentStyle,
	OldPriceStyle,
	NameStyle,
	ButtonBagStyle,
} from './card.style';

interface CardProps {
	name: string;
	price: number;
	discount: number;
	picture: string;
	wight: string;
}

export const Card: FC<CardProps> = ({
	name,
	price,
	discount,
	picture,
	wight,
}) => {
	return (
		<CardStyle>
			{discount > 0 && <PercentStyle>{`-${discount}%`}</PercentStyle>}
			<FavoritesStyle>
				<Icon name={'favoriteSecondary'} />
			</FavoritesStyle>
			<img width={'100%'} src={picture} alt={name} />
			<div>
				{discount > 0 && <OldPriceStyle>{price}</OldPriceStyle>}
				<Typography
					content={Math.floor(price - (price * discount) / 100).toString()}
					size={'xmd'}
					fontWeight={'xlg'}
					color={discount > 0 ? 'red' : 'black'}
				/>
				<Typography content={wight} color={'secondary'} />
				<NameStyle>
					<Typography size={'md'} fontWeight={'md'} content={name} />
				</NameStyle>
			</div>
			<ButtonBagStyle>в корзину</ButtonBagStyle>
		</CardStyle>
	);
};

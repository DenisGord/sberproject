import styled from 'styled-components';

export const FooterStyle = styled.footer(
	({ theme }) => `
	background-color: ${theme.colors.primary};
	padding: 40px 224px;
	display: flex;
	height: 100%;
	align-items:center;
	justify-content:space-between;
`
);

export const FlexContainerStyle = styled.div`
	display: flex;
	align-items: center;
	justify-content: center;
	gap: 12px;
`;

export const LiStyle = styled.li`
	list-style: none;
	margin-bottom: 10px;
`;

export const UlStyle = styled.ul`
	list-style: none;
	display: flex;
	flex-direction: column;
	flex-wrap: wrap;
	max-height: 114px;
	margin: 0;
	padding: 0;
`;

export const LinkStyle = styled.a`
	text-decoration: none;
	color: black;
	font-size: 12px;
	font-weight: 600;
`;

export const FlexColumnStyle = styled.div`
	display: flex;
	flex-direction: column;
	align-items: flex-start;
	gap: 16px;
`;

export const FlexColumnNoGapStyle = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	align-items: flex-start;
	align-self: stretch;
`;

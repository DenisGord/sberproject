import React, { useCallback, useMemo } from 'react';
import {
	FooterStyle,
	FlexContainerStyle,
	LiStyle,
	UlStyle,
	LinkStyle,
	FlexColumnStyle,
	FlexColumnNoGapStyle,
} from './footer.style';
import { Icon } from '../icon';
import { footerLinks, footerSocialNetworks } from '../../constants';
import { Typography } from '../typography';

export const Footer = () => {
	const renderPageLinks = useCallback((start: number, end: number) => {
		const arr = [];
		for (let i = start; i < end; i++) {
			const { title, url } = footerLinks[i];
			arr.push(
				<LiStyle key={title}>
					<LinkStyle href={url}>{title}</LinkStyle>
				</LiStyle>
			);
		}
		return arr;
	}, []);

	const renderSocialNetwork = useMemo(() => {
		return footerSocialNetworks.map(({ iconName, url }) => {
			return (
				<a key={iconName} href={url}>
					<Icon name={iconName} />
				</a>
			);
		});
	}, []);

	return (
		<FooterStyle>
			<FlexColumnNoGapStyle>
				<Icon name={'logotip'} />
				<Typography content={'© «Интернет-магазин DogFood.ru»'} />
			</FlexColumnNoGapStyle>
			<UlStyle>{renderPageLinks(0, 4)}</UlStyle>
			<UlStyle>{renderPageLinks(4, footerLinks.length)}</UlStyle>
			<FlexColumnStyle>
				<Typography size={'md'} content={'Мы на связи'} fontWeight={'lg'} />
				<div>
					<Typography
						size={'md'}
						content={'8 (999) 00-00-00'}
						fontWeight={'lg'}
					/>
					<Typography content={'dogfood.ru@gmail.com'} />
				</div>
				<FlexContainerStyle>{renderSocialNetwork}</FlexContainerStyle>
			</FlexColumnStyle>
		</FooterStyle>
	);
};

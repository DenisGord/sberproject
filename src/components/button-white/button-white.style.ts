import styled from 'styled-components';

export const ButtonStyle = styled.button(
	({ theme }) => `
	font-size:${theme.sizes.md};
	border-radius: 87px;
	border: 1px solid ${theme.colors.secondary};
	padding: ${theme.sizes.sm} ${theme.sizes.md};
	background:${theme.colors.white};
	cursor: pointer;
`
);

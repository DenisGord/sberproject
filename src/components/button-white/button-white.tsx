import React, { FC } from 'react';
import { ButtonStyle } from './button-white.style';

interface ButtonWhiteProps {
	content: string;
	onClick: () => void;
}
export const ButtonWhite: FC<ButtonWhiteProps> = ({ content, onClick }) => {
	return <ButtonStyle onClick={onClick}>{content}</ButtonStyle>;
};

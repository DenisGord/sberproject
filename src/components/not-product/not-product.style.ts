import styled from 'styled-components';
export const NotProductStyle = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
	gap: 20px;
	max-width: 250px;
	justify-content: center;
	text-align: center;
`;

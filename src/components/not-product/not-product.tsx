import React from 'react';
import { Icon } from '../icon';
import { Typography } from '../typography';
import { NotProductStyle } from './not-product.style';
import { ButtonWhite } from '../button-white';

export const NotProduct = () => {
	return (
		<NotProductStyle>
			<Icon name={'notFound'} />
			<Typography
				content={'Простите, по вашему запросу\n' + 'товаров не надено.'}
				fontWeight={'lg'}
				size={'md'}
			/>
			<ButtonWhite
				onClick={() => console.log('переход на главную')}
				content={'На главную'}
			/>
		</NotProductStyle>
	);
};

import React, { FC } from 'react';

interface MarginWrapperProps {
	children: JSX.Element;
	marginTop?: string;
	marginLeft?: string;
	marginBottom?: string;
	marginRight?: string;
}

export const MarginWrapper: FC<MarginWrapperProps> = ({
	children,
	marginTop = '0px',
	marginLeft = '0px',
	marginBottom = '0px',
	marginRight = '0px',
}) => {
	return (
		<div style={{ marginRight, marginLeft, marginTop, marginBottom }}>
			{children}
		</div>
	);
};

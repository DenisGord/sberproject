import React, { FC, useCallback } from 'react';
import { SearchContainerStyle, InputSearchStyle } from './search.style';
import { Icon } from '../icon';
import { ButtonCross } from '../button-cross';

interface SearchProps {
	value: string;
	setValue: (key: string) => void;
}
export const Search: FC<SearchProps> = ({ value, setValue }) => {
	const clearValue = useCallback(() => {
		setValue('');
	}, [setValue]);
	return (
		<SearchContainerStyle>
			<InputSearchStyle
				value={value}
				placeholder={'Поиск'}
				onChange={({ target }) => setValue(target.value)}
			/>
			{!value.length ? (
				<Icon name={'search'} />
			) : (
				<ButtonCross onClick={clearValue} />
			)}
		</SearchContainerStyle>
	);
};

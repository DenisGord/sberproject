import styled from 'styled-components';

export const SearchContainerStyle = styled.label(
	({ theme }) => `
	border-radius: 24px;
	padding: 12px 20px;
	align-items: center;
	display: flex;
	background-color: ${theme.colors.white};
	flex: 1 0 0;
	min-width:200px;
`
);
export const InputSearchStyle = styled.input`
	border: none;
	width: calc(100% - 20px);
	outline: none;
`;

import React, { FC } from 'react';
import { HeaderStyle } from './header.style';
import { Search } from '../search';
import { Icon } from '../icon';
import { MarginWrapper } from '../margin-wrapper/margin-wrapper';

interface HeaderProps {
	searchValue: string;
	setSearchValue: (key: string) => void;
}
export const Header: FC<HeaderProps> = ({ searchValue, setSearchValue }) => {
	return (
		<HeaderStyle>
			<Icon name={'logotip'} />
			<Search value={searchValue} setValue={setSearchValue} />
			<div style={{ display: 'flex', alignItems: 'center' }}>
				<Icon name={'favorites'} />
				<MarginWrapper marginLeft={'34px'}>
					<Icon name={'bag'} />
				</MarginWrapper>
				<MarginWrapper marginLeft={'34px'}>
					<Icon name={'dogFace'} />
				</MarginWrapper>
			</div>
		</HeaderStyle>
	);
};

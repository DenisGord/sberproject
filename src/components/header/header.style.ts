import styled from 'styled-components';

export const HeaderStyle = styled.header(
	({ theme }) => `
	height: 80px;
	background-color: ${theme.colors.primary};
	display: flex;
	align-items: center;
	justify-content: center;
	gap:80px;
	padding: 12px 224px;
`
);

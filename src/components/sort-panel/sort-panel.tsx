import React, { useMemo, useState } from 'react';
import { SortPanelStyle, LiStyle } from './sort-panel.style';
import { sortData } from '../../constants';
import { Typography } from '../typography';

export const SortPanel = () => {
	const [active, setActive] = useState(sortData[0].code);
	const renderSortList = useMemo(() => {
		return sortData.map(({ title, code }) => {
			return (
				<LiStyle key={title} onClick={() => setActive(code)}>
					<Typography
						content={title}
						color={code === active ? 'black' : 'secondary'}
						size={'md'}
						fontWeight={'sm'}
					/>
				</LiStyle>
			);
		});
	}, [active]);

	return <SortPanelStyle>{renderSortList}</SortPanelStyle>;
};

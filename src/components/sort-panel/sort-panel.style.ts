import styled from 'styled-components';

export const SortPanelStyle = styled.ul(
	({ theme }) => `
	display:flex;
	align-items:center;
	list-style: none;
	padding: ${theme.sizes.xsm} ${theme.sizes.md};
	gap:16px;
	border-radius:${theme.sizes.sm};
	width:100%;
	box-shadow: 0px 8px 16px 0px rgba(96, 97, 112, 0.16), 0px 2px 4px 0px rgba(40, 41, 61, 0.04);
	`
);

export const LiStyle = styled.li`
	cursor: pointer;
`;

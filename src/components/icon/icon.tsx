import React, { FC } from 'react';
import * as Icons from '../../assets/icons';

interface IconProps {
	name: string;
}

export const Icon: FC<IconProps> = ({ name }) => {
	// eslint-disable-next-line import/namespace
	const icon = Icons[name as keyof typeof Icons];

	if (icon) {
		return <img src={icon} alt={name} />;
	}
	return null;
};

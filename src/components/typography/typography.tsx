import React, { FC } from 'react';
import { TypographyStyle } from './typography.style';

interface TypographyProps {
	size?: 'sm' | 'md' | 'lg' | 'xmd';
	color?: 'black' | 'secondary' | 'red';
	content: string;
	fontWeight?: 'sm' | 'md' | 'lg' | 'xlg';
}
export const Typography: FC<TypographyProps> = ({
	size = 'sm',
	color = 'black',
	content,
	fontWeight = 'sm',
}) => {
	return (
		<TypographyStyle fontWeight={fontWeight} size={size} color={color}>
			{content}
		</TypographyStyle>
	);
};

import styled from 'styled-components';

interface props {
	size: 'sm' | 'lg' | 'md' | 'xmd';
	color: 'black' | 'secondary' | 'red';
	fontWeight: 'sm' | 'lg' | 'md' | 'xlg';
}

export const TypographyStyle = styled.p<props>(
	({ size, color, theme, fontWeight }) => `
	color:${theme.colors[color]};
	font-size:${theme.sizes[size]};
	font-weight:${theme.fontWeight[fontWeight]};
	padding:0;
	margin:0;
`
);

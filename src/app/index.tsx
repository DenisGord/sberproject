import './styles.css';
import { Header } from '../components/header';
import { Footer } from '../components/footer';
import { WrapperPageStyle, ContentStyle } from './index.style';
import { Card } from '../components/card';
import Data from '../constants/data.json';
import { SortPanel } from '../components/sort-panel';
import { Loader } from '../components/loader';
import { useEffect, useState } from 'react';
import { NotProduct } from '../components/not-product';
import { Pagination } from '../components/pagination';

interface DataType {
	name: string;
	price: number;
	discount: number;
	wight: string;
	description: string;
	isFavorite: boolean;
	isCart: boolean;
	available: boolean;
	stock: number;
	picture: string;
}
export const App = () => {
	const [data, setData] = useState<DataType[]>(Data);
	const [searchValue, setSearchValue] = useState('');
	const [loader, setLoader] = useState(true);
	const [paginationPage, setPaginationPage] = useState(0);
	const [paginationListData, setPaginationListData] = useState<DataType[]>([]);

	useEffect(() => {
		setTimeout(() => {
			setLoader(false);
		}, 2000);
	}, []);

	useEffect(() => {
		setPaginationPage(0);
		setData(
			Data.filter(({ name }) =>
				name.toLowerCase().includes(searchValue.toLowerCase())
			)
		);
	}, [searchValue]);

	useEffect(() => {
		setPaginationListData(
			data.slice(paginationPage * 12, paginationPage * 12 + 12)
		);
	}, [data, paginationPage]);

	return (
		<WrapperPageStyle>
			<Header searchValue={searchValue} setSearchValue={setSearchValue} />
			<ContentStyle>
				{loader ? (
					<Loader />
				) : data.length ? (
					<>
						<SortPanel />
						{paginationListData.map((item) => {
							return <Card key={item.name} {...item} />;
						})}
						<Pagination
							current={paginationPage}
							setNum={setPaginationPage}
							quantity={data.length}
						/>
					</>
				) : (
					<NotProduct />
				)}
			</ContentStyle>
			<Footer />
		</WrapperPageStyle>
	);
};

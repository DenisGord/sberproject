import styled from 'styled-components';

export const WrapperPageStyle = styled.div`
	display: flex;
	flex-direction: column;
	min-height: 100vh;
`;

export const ContentStyle = styled.div`
	flex-grow: 1;
	min-height: 100%;
	display: flex;
	flex-wrap: wrap;
	gap: 16px;
	margin: 0 auto;
	max-width: 994px;
	padding-bottom: 20px;
	align-items: center;
`;
